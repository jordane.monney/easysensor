#include "mbed.h"

DigitalOut myled1(LED1);
DigitalOut myled2(LED2);
DigitalOut myled3(LED3);
DigitalOut myled4(LED4);

int main() {
    // All LEDs are OFF
    myled1 = 1;
    myled2 = 1;
    myled3 = 1;
    myled4 = 1;
}
