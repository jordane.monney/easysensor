﻿# PS5 - Easy Sensor

**Projet de semestre 5 - Jordane Monney** <br/>
<br>
Tous les documents concernants mon projet de semestre 5 (*Easy Sensor*) sont déposés ici. Cela comprend :
*  Le rapport final du projet
*  Les prcocès verbaux de chaque séance hebdomadaire
*  Les scripts python
*  Les deux programmes pour la plateforme STM
*  Les mesures avec les photos des différents emplacements de tests
*  Liste des tâches